package eu.playperium.votelistener.commands;

import eu.playperium.votelistener.permissions.PluginPermissions;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.action.TextActions;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.format.TextStyles;

import java.net.URL;

public class CommandVote implements CommandExecutor {

    public CommandSpec commandSpec = CommandSpec.builder().permission(PluginPermissions.COMMAND_VOTE).executor(this).build();

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        if (src instanceof Player) {
            Player player = (Player) src;

            try {
                URL voteLink1 = new URL("http://vote1.playperium.eu");
                URL voteLink2 = new URL("http://vote2.playperium.eu");

                player.sendMessage(Text.builder("-----------------------------------------------------").color(TextColors.GREEN).build());
                player.sendMessage(Text.builder("").build());
                player.sendMessage(Text.builder(" Vote für unseren Server um eine Belohnung zu erhalten!").color(TextColors.GOLD).build());
                player.sendMessage(
                        Text.builder("  <>").color(TextColors.GREEN).style(TextStyles.BOLD).append(
                                Text.builder(" http://vote1.playperium.eu").color(TextColors.AQUA).style(TextStyles.RESET).onClick(TextActions.openUrl(voteLink1)).append(
                                        Text.builder(" (Auf den Link klicken)").color(TextColors.GRAY).build()
                                ).build()).build());

                player.sendMessage(
                        Text.builder("  <>").color(TextColors.GREEN).style(TextStyles.BOLD).append(
                                Text.builder(" http://vote2.playperium.eu").color(TextColors.AQUA).style(TextStyles.RESET).onClick(TextActions.openUrl(voteLink2)).append(
                                        Text.builder(" (Auf den Link klicken)").color(TextColors.GRAY).build()
                                ).build()).build());

                player.sendMessage(Text.builder(" Du erhältst: ").color(TextColors.BLUE).append(
                        Text.builder("150 Claim Blöcke").color(TextColors.GOLD)
                                .build()).build());

                player.sendMessage(Text.builder("").build());
                player.sendMessage(Text.builder("-----------------------------------------------------").color(TextColors.GREEN).build());
            } catch (Exception e) {

            }
        }

        return CommandResult.success();
    }
}
