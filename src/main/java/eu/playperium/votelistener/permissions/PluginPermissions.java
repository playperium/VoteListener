package eu.playperium.votelistener.permissions;

public final class PluginPermissions {

    public static final String COMMAND_VOTE = "pp.votelistener.command.vote";
}
