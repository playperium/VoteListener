package eu.playperium.votelistener;

import com.google.inject.Inject;
import com.vexsoftware.votifier.model.Vote;
import com.vexsoftware.votifier.sponge.event.VotifierEvent;
import eu.playperium.votelistener.commands.CommandVote;
import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.plugin.Dependency;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.format.TextStyles;

@Plugin(
        id = "vote-listener",
        name = "Vote Listener",
        dependencies = {@Dependency(id = "nuvotifier")}
)
public class VoteListener {

    @Inject
    private Logger logger;

    @Listener
    public void onServerStart(GameStartedServerEvent event) {
        registerCommands();
    }

    private void registerCommands() {
        Sponge.getCommandManager().register(this, new CommandVote().commandSpec, "vote");
    }

    @Listener
    public void voteListener(VotifierEvent e) {
        Vote vote = e.getVote();

        for (Player player : Sponge.getServer().getOnlinePlayers()) {
            if (player.getName().equalsIgnoreCase(vote.getUsername())) {

                Text voteBroadcastMessage = Text.builder(player.getName()).color(TextColors.DARK_PURPLE).style(TextStyles.BOLD).append(
                        Text.builder(" hat gevotet und eine").color(TextColors.AQUA).style(TextStyles.RESET).append(
                                Text.builder(" Belohnung").color(TextColors.GREEN).style(TextStyles.BOLD).append(
                                        Text.builder(" erhalten!").color(TextColors.AQUA).style(TextStyles.RESET).append(
                                                Text.builder(" /vote").color(TextColors.GOLD).style(TextStyles.BOLD)
                                                        .build()).build()).build()).build()).build();

                Sponge.getServer().getBroadcastChannel().send(voteBroadcastMessage);

                Text rewardPlayerMessage = Text.builder("Du hast ").color(TextColors.AQUA).append(
                        Text.builder("150 Claim Blöcke").color(TextColors.GREEN).style(TextStyles.BOLD).append(
                                Text.builder(" erhalten!").color(TextColors.AQUA).style(TextStyles.RESET)
                                        .build()).build()).build();

                player.sendMessage(rewardPlayerMessage);

                Sponge.getCommandManager().process(Sponge.getServer().getConsole(), "adjustbonusclaimblocks " + player.getName() + " 150");
            }
        }
    }
}
